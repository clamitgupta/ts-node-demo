import { Document, Model, model, Schema } from 'mongoose';
import { Companies } from './companies.interface';

export interface CompanyModel extends Companies, Document { }

export const CompaniesSchema: Schema = new Schema({
    createdAt: Date,
    isin: { type: String },
    link: { type: String },
    name: String,
});

CompaniesSchema.pre('save', (next) =>  {
    const now = new Date();
    if (!this.createdAt) {
      this.createdAt = now;
    }
    next();
});

export const Company: Model<CompanyModel> = model<CompanyModel>('Company', CompaniesSchema);
