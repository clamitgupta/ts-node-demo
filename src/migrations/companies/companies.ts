import * as async from 'async';
import { getLogger } from 'log4js';
import { Company } from './companies.model';
import { companies } from './companies.seed';

const logger = getLogger();
logger.level = process.env.LOG_LEVEL;

const company = companies();

export function insertCompaniesData() {
    async.each(company, (x, callback) => {
        Company.find({ isin: x.isin }, (err, results) => {
            if (err) {
                callback('Error while querying data for ' + x.name);
            } else {
                if (results.length) {
                    logger.info('Already inserted data for ' + x.name);
                    callback();
                } else {
                    Company.create(x).then((response) => {
                        logger.info('Data inserted for ' + x.name);
                        callback();
                    }, (error) => {
                        callback('Error while inserting data for ' + x.name);
                    });
                }
            }
        });
    }, (err) => {
        if (err) {
            console.log(err);
        } else {
            logger.info('All companies data inserted succesfully');
        }
        process.exit();
    });
}
