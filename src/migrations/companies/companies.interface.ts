export interface Companies {
    isin: string;
    name?: string;
    link: string;
}
