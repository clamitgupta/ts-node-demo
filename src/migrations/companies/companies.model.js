"use strict";
var _this = this;
exports.__esModule = true;
var mongoose_1 = require("mongoose");
exports.CompaniesSchema = new mongoose_1.Schema({
    createdAt: Date,
    isin: { type: String },
    link: { type: String },
    name: String
});
exports.CompaniesSchema.pre('save', function (next) {
    var now = new Date();
    if (!_this.createdAt) {
        _this.createdAt = now;
    }
    next();
});
exports.Company = mongoose_1.model('Company', exports.CompaniesSchema);
