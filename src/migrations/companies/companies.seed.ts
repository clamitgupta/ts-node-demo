export function companies() {
  return [{
      isin: 'US88579Y1010',
      link: '3m-co',
      name: '3M Company',
    }, {
      isin: 'US0258161092 ',
      link: 'american-express',
      name: 'American Express Company',
    }, {
      isin: 'US0378331005',
      link: 'apple-computer-inc',
      name: 'Apple Inc',
    }, {
      isin: 'US0970231058',
      link: 'boeing-co',
      name: 'Boeing Co',
    }, {
      isin: 'US1491231015',
      link: 'caterpillar',
      name: 'Caterpillar Inc',
    }, {
      isin: 'US1667641005',
      link: 'chevron',
      name: 'Chevron Corp',
    }, {
      isin: 'US17275R1023',
      link: 'cisco-sys-inc',
      name: 'Cisco Systems Inc',
    }, {
      isin: 'US1912161007',
      link: 'coca-cola-co',
      name: 'Coca-Cola Company',
    }, {
      isin: 'US26078J1007',
      link: 'du-pont',
      name: 'DowDuPont Inc',
    }, {
      isin: 'US30231G1022',
      link: 'exxon-mobil',
      name: 'Exxon Mobil Corp',
    }, {
      isin: 'US38141G1040',
      link: 'goldman-sachs-group',
      name: 'Goldman Sachs Group Inc',
    }, {
      isin: 'US4370761029',
      link: 'home-depot',
      name: 'Home Depot Inc',
    }, {
      isin: 'US4592001014',
      link: 'ibm',
      name: 'International Business Machines',
    }, {
      isin: 'US4581401001',
      link: 'intel-corp',
      name: 'Intel Corporation',
    }, {
      isin: 'US4781601046',
      link: 'johnson-johnson',
      name: 'Johnson & Johnson',
    }, {
      isin: 'US46625H1005',
      link: 'jp-morgan-chase',
      name: 'JPMorgan Chase & Co',
    }, {
      isin: 'US5801351017',
      link: 'mcdonalds',
      name: 'McDonald’s Corporation',
    }, {
      isin: 'US58933Y1055',
      link: 'merck---co',
      name: 'Merck & Company Inc',
    }, {
      isin: 'US5949181045',
      link: 'microsoft-corp',
      name: 'Microsoft Corporation',
    }, {
      isin: 'US6541061031',
      link: 'nike',
      name: 'Nike Inc',
    }, {
      isin: 'US7170811035',
      link: 'pfizer',
      name: 'Pfizer Inc',
    }, {
      isin: 'US7427181091',
      link: 'procter-gamble',
      name: 'Procter & Gamble Company',
    }, {
      isin: 'US89417E1091',
      link: 'the-travelers-co',
      name: 'The Travelers Companies Inc',
    }, {
      isin: 'US9130171096',
      link: 'united-tech',
      name: 'United Technologies Corporation',
    }, {
      isin: 'US91324P1021',
      link: 'united-health-group',
      name: 'UnitedHealth Group Incorporated',
    }, {
      isin: 'US92343V1044',
      link: 'verizon-communications',
      name: 'Verizon Communications Inc',
    }, {
      isin: 'US92826C8394',
      link: 'visa-inc',
      name: 'Visa Inc',
    }, {
      isin: 'US9314271084',
      link: 'walgreen-co',
      name: 'Walgreens Boots Alliance Inc',
    }, {
      isin: 'US9311421039',
      link: 'wal-mart-stores',
      name: 'Walmart Inc',
    }, {
      isin: 'US2546871060',
      link: 'disney',
      name: 'Walt Disney Company',
    },
  ];
}
