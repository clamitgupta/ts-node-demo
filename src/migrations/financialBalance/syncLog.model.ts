
import { Document, Model, model, Schema } from 'mongoose';
import { Sync } from './syncLog.interface';

export interface SyncModel extends Sync, Document { }

export const SyncSchema: Schema = new Schema({
    count: Number,
    createdAt: Date,
    type: String,
    updatedAt: Date,
});

SyncSchema.pre('save', (next) =>  {
    const now = new Date();
    if (!this.createdAt) {
      this.createdAt = now;
    }
    this.updatedAt = now;
    next();
});

export const sync: Model<SyncModel> = model<SyncModel>('Sync', SyncSchema);
