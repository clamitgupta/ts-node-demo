export interface FinancialBalance {
    field: object;
    periodEnding: object;
    quarter: number;
}
