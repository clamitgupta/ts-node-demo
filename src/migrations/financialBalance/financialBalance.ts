import * as async from 'async';
import { getLogger } from 'log4js';
import * as request from 'request';
import { Company } from '../companies/companies.model';
import { FinanceBalance } from './financialBalance.model';
import { sync } from './syncLog.model';

const logger = getLogger();
logger.level = process.env.LOG_LEVEL;

export function importFinances(): Promise<any> {
    return new Promise((resolve, reject) => {
        async.waterfall([
            (callback) => {
                sync.findOne({ type: 'financeBalance' }, 'count', (e, x) =>  {
                    const cnt: number = x ? x.count : 0;
                    return callback(null, cnt);
                });
            },
            (cnt, callback) => {
                const query = Company.find(({})).skip(cnt).limit(1);
                query.exec((error, data) => {
                    if (error) {
                        return callback(error);
                    }
                    return callback(null, data[0], cnt);
                });
            },
            (data, cnt, callback) => {
                if (data && data.link) {
                    request(
                        process.env.SERVERPORT + 'investing/%2Fequities%2F'
                        + data.link + '/financials/balance', (error, response, body) =>  {
                            if (error === null && response.statusCode === 200) {
                                body = JSON.parse(body);
                                return callback(null, body, data, cnt);
                            } else {
                                return callback('Navigation Timeout Exceeded: 30000ms exceeded.');
                            }
                        });
                } else {
                    return callback('No more data to insert.');
                }
            },
            (body, data, cnt, callback) => {
                body.balance.forEach((element) => {
                    element.periodEnding.value = new Date(element.periodEnding.value);
                    element.company = data._id;
                });
                FinanceBalance.insertMany(body.balance, (err) => {
                    if (err) {
                        return callback(err);
                    }
                    return callback(null, cnt);
                });
            },
            (cnt, callback) => {
                cnt = cnt + 1;
                sync.findOneAndUpdate(
                    { type: 'financeBalance' },
                    {
                        count: cnt,
                        createdAt: new Date(),
                        type: 'financeBalance',
                        updatedAt: new Date(),
                    },
                    { upsert: true },
                    (err) =>  {
                        if (err) {
                            return callback(err);
                        } else {
                            return callback(null, 'Data added successfully');
                        }
                });
            },
        ], (err, result) =>  {
            if (err) {
                return resolve({
                    message: err,
                    status: 'error',
                });
            } else {
                return resolve({
                    message: result,
                    status : 'success',
                });
            }
        });
    });
}
