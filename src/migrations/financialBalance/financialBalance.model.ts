import { Document, Model, model, Schema } from 'mongoose';
import { FinancialBalance } from './financialBalance.interface';

export interface FinancialBalanceModel extends FinancialBalance, Document { }

export const FinancialBalanceSchema: Schema = new Schema({
    company: { type: Schema.Types.ObjectId, ref: 'Company' },
    field: {
        id: Number,
        name: String,
        originalValue: String,
        parentId: {
            default: null,
            type: Number,
        },
        value: Number,
    },
    periodEnding: {
        originalValue: String,
        value: Date,
    },
    quarter: Number,
});

FinancialBalanceSchema.pre('save', (next) =>  {
    const now = new Date();
    if (!this.createdAt) {
      this.createdAt = now;
    }
    next();
});

export const FinanceBalance: Model<FinancialBalanceModel>
= model<FinancialBalanceModel>('FinanceBalance', FinancialBalanceSchema);
