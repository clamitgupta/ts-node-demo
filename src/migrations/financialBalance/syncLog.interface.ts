export interface Sync {
    type: string;
    count: number;
}
