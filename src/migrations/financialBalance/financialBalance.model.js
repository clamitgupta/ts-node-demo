"use strict";
var _this = this;
exports.__esModule = true;
var mongoose_1 = require("mongoose");
exports.FinancialBalanceSchema = new mongoose_1.Schema({
    company: { type: mongoose_1.Schema.Types.ObjectId, ref: 'Company' },
    field: {
        id: Number,
        name: String,
        originalValue: String,
        parentId: {
            "default": null,
            type: Number
        },
        value: Number
    },
    periodEnding: {
        originalValue: String,
        value: Date
    },
    quarter: Number
});
exports.FinancialBalanceSchema.pre('save', function (next) {
    var now = new Date();
    if (!_this.createdAt) {
        _this.createdAt = now;
    }
    next();
});
exports.FinanceBalance = mongoose_1.model('FinanceBalance', exports.FinancialBalanceSchema);
