import * as dotenv from 'dotenv';
import { getLogger } from 'log4js';
import * as mongoose from 'mongoose';
import { insertCompaniesData } from './src/migrations/companies/companies';
import { importFinances } from './src/migrations/financialBalance/financialBalance';

dotenv.config();

const logger = getLogger();
logger.level = process.env.LOG_LEVEL;

mongoose.connect(process.env.MONGOURL, {useNewUrlParser: true});
mongoose.connection.on('connected', () => {
    logger.info('connected');
    const flag = process.argv[2];
    if (flag === '--companies') {
        insertCompaniesData();
    } else if (flag === '--finances') {
        importFinances().then((response) => {
            logger.info('Cycle completed');
            if (response.status === 'success') {
                logger.info(response);
                importFinances();
            } else {
                logger.error(response);
                process.exit();
            }
        });
    } else {
        logger.error('no flag defined');
        process.exit();
    }
});
