"use strict";
exports.__esModule = true;
var dotenv = require("dotenv");
var express = require("express");
var http = require("http");
var log4js_1 = require("log4js");
var mongoose = require("mongoose");
var finances_1 = require("./routers/finances");
dotenv.config();
var logger = log4js_1.getLogger();
logger.level = process.env.LOG_LEVEL;
mongoose.connect(process.env.MONGOURL, { useNewUrlParser: true });
mongoose.connection.on('connected', function () {
    logger.info('connected');
});
var port = process.env.PORT;
var app = express();
app.set('case sensitive routing', false);
app.set('json spaces', 2);
app.use('/finance', finances_1["default"]);
app.get('*', function (req, res) {
    res.status(404).send({
        error: {
            message: 'No route found to handle such request.',
            type: 'no_route_found'
        }
    });
});
http.createServer(app).listen(port, function _() {
    logger.info('Listening http on:', port);
});
