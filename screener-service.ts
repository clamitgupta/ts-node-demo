import * as dotenv from 'dotenv';
import * as express from 'express';
import * as http from 'http';
import { getLogger } from 'log4js';
import * as mongoose from 'mongoose';
import finance from './routers/finances';

dotenv.config();

const logger = getLogger();
logger.level = process.env.LOG_LEVEL;

mongoose.connect(process.env.MONGOURL, {useNewUrlParser: true});

mongoose.connection.on('connected', () => {
  logger.info('connected');
});

const port = process.env.PORT;

const app = express();

app.set('case sensitive routing', false);
app.set('json spaces', 2);

app.use('/finance', finance);

app.get('*', (req, res) => {
  res.status(404).send({
    error: {
      message: 'No route found to handle such request.',
      type: 'no_route_found',
    },
  });
});

http.createServer(app).listen(port, function _() {
  logger.info('Listening http on:', port);
});
