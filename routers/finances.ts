import * as express from 'express';
import { Company } from '../src/migrations/companies/companies.model';
import { FinanceBalance } from '../src/migrations/financialBalance/financialBalance.model';

const router = express.Router();
export default router;

router.get('/', (req, res, next) => {
    try {
        const name = req.query.name ? req.query.name : null;
        if ( !name ) {
            return res.status(400).send({
                error: {
                    message: 'Insufficient parameters',
                    type: 'invalid_request_error',
                },
            });
        }

        FinanceBalance
        .find({
            'field.name' : {
                $options: 'i',
                $regex : name,
            },
        })
        .populate({
            model: Company,
            path: 'company',
            select: 'name link isin',
        })
        .exec((err, results) => {
            if (err) {
                return res.status(400).send({
                    error: {
                        message: err.message,
                        type: 'mongo_query_error',
                    },
                });
            } else {
                return res.send(results);
            }
        });
    } catch (e) {
        return res.status(400).send({
            error: {
                message: e.message,
                type: 'invalid_request_error',
            },
        });
    }
});
