"use strict";
exports.__esModule = true;
var express = require("express");
var companies_model_1 = require("../src/migrations/companies/companies.model");
var financialBalance_model_1 = require("../src/migrations/financialBalance/financialBalance.model");
var router = express.Router();
exports["default"] = router;
router.get('/', function (req, res, next) {
    try {
        var name_1 = req.query.name ? req.query.name : null;
        if (!name_1) {
            return res.status(400).send({
                error: {
                    message: 'Insufficient parameters',
                    type: 'invalid_request_error'
                }
            });
        }
        financialBalance_model_1.FinanceBalance
            .find({
            'field.name': {
                $options: 'i',
                $regex: name_1
            }
        })
            .populate({
            model: companies_model_1.Company,
            path: 'company',
            select: 'name link isin'
        })
            .exec(function (err, results) {
            if (err) {
                return res.status(400).send({
                    error: {
                        message: err.message,
                        type: 'mongo_query_error'
                    }
                });
            }
            else {
                return res.send(results);
            }
        });
    }
    catch (e) {
        return res.status(400).send({
            error: {
                message: e.message,
                type: 'invalid_request_error'
            }
        });
    }
});
